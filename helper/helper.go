package helper

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/gabey14/go-mongodb-api/model"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// NOTE MONGODB helpers - file

// TODO alias for string
// type key string

// const (
// 	hostKey     = key("hostKey")
// 	usernameKey = key("usernameKey")
// 	passwordKey = key("passwordKey")
// 	databaseKey = key("databaseKey")
// )

// Database Connection string
// const connectionString = "mongodb+srv://learncodeonline:ABgeorge14@cluster0.nlhsz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

const dbName = "netflix"
const colName = "watchlist"

// MOST IMPORTANT

var collection *mongo.Collection

// connect with mongoDB
func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	connectionString := os.Getenv("MONGODB_URI")
	// client option
	clientOption := options.Client().ApplyURI(connectionString)

	// connect to mongoDB
	client, err := mongo.Connect(context.TODO(), clientOption)

	if err != nil {
		log.Fatal(err)
	}

	// ping the database to make sure the uri is correct
	err = client.Ping(context.TODO(), readpref.Primary())
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("MongoDB connection success")
	}

	collection = client.Database(dbName).Collection(colName)

	// collection instance/reference
	fmt.Println("Collection instance is ready")

}

// insert 1 record can return *mongo.InsertOneResult

func InsertOneMovie(movie model.Netflix) {
	inserted, error := collection.InsertOne(context.Background(), movie)

	if error != nil {
		log.Fatal(error)
	}

	fmt.Println("Inserted 1 movie in db with id :", inserted.InsertedID)
}

// update 1 record

func UpdateOneMovie(movieId string) {

	id, error := primitive.ObjectIDFromHex(movieId)
	if error != nil {
		log.Fatal(error)
	}

	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{"watched": true}}

	result, err := collection.UpdateOne(context.Background(), filter, update)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Modified Count :", result.ModifiedCount)
}

// delete 1 record

func DeleteOneMovie(movieId string) {

	id, err := primitive.ObjectIDFromHex(movieId)

	if err != nil {
		log.Fatal(err)
	}

	filter := bson.M{"_id": id}

	deleteCount, err := collection.DeleteOne(context.Background(), filter)

	if err != nil {
		log.Fatal(err)
	}
	// [x] print the deletecount properly
	// fmt.Println("Movie got deleted with delete count:", deleteCount)
	fmt.Println("Movie got deleted with delete count:", deleteCount.DeletedCount)

}

// Delete all records from MONGODB

func DeleteAllMovie() int64 {
	// D is ordered representation and M is unordered representation
	deleteResult, err := collection.DeleteMany(context.Background(), bson.D{{}}, nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Number of movies deleted:", deleteResult.DeletedCount)
	return deleteResult.DeletedCount

}

// get all movies from database

func GetAllMovies() []primitive.M {
	cur, err := collection.Find(context.Background(), bson.D{{}})

	if err != nil {
		log.Fatal(err)
	}

	var movies []primitive.M

	for cur.Next(context.Background()) {
		var movie bson.M
		err := cur.Decode(&movie)
		if err != nil {
			log.Fatal(err)
		}
		movies = append(movies, movie)
	}

	defer cur.Close(context.Background())
	return movies

}
