package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gabey14/go-mongodb-api/router"
)

func main() {
	port := os.Getenv("PORT")
	fmt.Println("MongoDB API in Golang")
	r := router.Router()
	fmt.Println("Server is getting started... on port 4000")

	log.Fatal(http.ListenAndServe(":"+port, r))

}
