package controller

// controllers & helpers
import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gabey14/go-mongodb-api/helper"
	"github.com/gabey14/go-mongodb-api/model"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// NOTE controller - file

func GetMyAllMovies(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Content-Type", "application/x-www-form-urlencode")

	allMovies := helper.GetAllMovies()

	json.NewEncoder(w).Encode(allMovies)

}

func CreatMovie(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/x-www-form-urlencode")

	w.Header().Set("Allow-Control-Allow-Methods", "POST")

	var movie model.Netflix

	err := json.NewDecoder(r.Body).Decode(&movie)

	if err != nil {
		log.Fatal(err)
	}
	//[x] return the object id properly
	movie.ID = primitive.NewObjectID()
	// inserted_id := insertOneMovie(movie)
	helper.InsertOneMovie(movie)
	json.NewEncoder(w).Encode(movie)
	// json.NewEncoder(w).Encode(inserted_id.InsertedID)

}

func MarkAsWatched(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencode")

	w.Header().Set("Allow-Control-Allow-Methods", "PUT")

	parans := mux.Vars(r)

	helper.UpdateOneMovie(parans["id"])
	json.NewEncoder(w).Encode(parans["id"])

}

func DeleteAMovie(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencode")
	w.Header().Set("Allow-Control-Allow-Methods", "DELETE")

	params := mux.Vars(r)

	helper.DeleteOneMovie(params["id"])
	json.NewEncoder(w).Encode(params["id"])
}

func DeleteAllMovies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencode")
	w.Header().Set("Allow-Control-Allow-Methods", "DELETE")

	count := helper.DeleteAllMovie()
	json.NewEncoder(w).Encode(count)
}
